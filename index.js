const express = require("express");
const mongoose = require("mongoose");
const cookiParser = require("cookie-parser");
const dotenv = require("dotenv");
dotenv.config();
const cors = require("cors");
const Routes = require("./Routes/index");
const { ConnectDb } = require("./Configration/Database");
const PORT = process.env.PORT;
const app = express();
// middleWares 
app.use(express.json());
app.use(
  cors({
    origin:process.env.HOME_URL,
    methods: ["POST", "GET"],
    credentials: true,
  })
);
// Add a middleware to explicitly allow the required headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', process.env.HOME_URL);
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  res.header('Access-Control-Allow-Credentials', 'true');
  next();
});
app.use(cookiParser());
app.use(express.urlencoded({ extended: false }));
app.use("/", Routes);

ConnectDb().then(() => {
  app.listen(PORT, () => {
    console.log(`Connected Server In PORT : ${PORT}`);
  });
});
